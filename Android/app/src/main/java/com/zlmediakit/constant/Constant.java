package com.zlmediakit.constant;

/**
 * @AUTHOR fengyook
 * @DATE 2021/11/9 17:35
 * @DES
 */
public class Constant {

    public static final String MACHINE_IP = "10.0.0.53";
    public static final String PORT = "";
    public static final String vhost = "__defaultVhost__";
    public static final String secret = "035c73f7-bb6b-4889-a715-d9eb2d1925cc";
    public static final String stream_id = "1234";

}
