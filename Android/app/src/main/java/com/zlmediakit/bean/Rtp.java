package com.zlmediakit.bean;

/**
 * @AUTHOR fengyook
 * @DATE 2021/11/3 14:08
 * @DES
 */
public class Rtp {
    int code;
    int port;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "Rtp{" +
                "code=" + code +
                ", port=" + port +
                '}';
    }

    public void setPort(int port) {
        this.port = port;
    }


}
