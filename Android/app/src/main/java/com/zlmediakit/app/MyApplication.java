package com.zlmediakit.app;

import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.zlmediakit.service.MediaService;

/**
 * @AUTHOR fengyook
 * @DATE 2021/11/1 16:40
 * @DES
 */
public class MyApplication extends Application {

    String TAG = "MyApplication";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
    }


}
