package com.zlmediakit.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.zlmediakit.R;
import com.zlmediakit.bean.Rtp;
import com.zlmediakit.constant.Constant;
import com.zlmediakit.jni.ZLMediaKit;
import com.zlmediakit.service.MediaService;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * @author Administrator
 */
@SuppressLint("HandlerLeak")
public class MainActivity extends Activity {
    public static final String TAG = "ZLMediaKit";
    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.INTERNET"};

    Context context;
    private ZLMediaKit.MediaPlayer _player;
    private ZLMediaKit.MediaPlayerCallBack mediaPlayerCallBack;
    private ZLMediaKit.MediaFrame _mediaFrame;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        startMediaService();
//        surface = findViewById(R.id.surfaceview);
//        surfaceHolder = surface.getHolder();
//        surfaceHolder.setFormat(SurfaceHolder.SURFACE_TYPE_HARDWARE);
    }

    /***
     * 初始化media server
     */
    private void startMediaService() {
        //后台启动service
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O) {
            Intent serviceIntent = new Intent(this, MediaService.class);
            startForegroundService(serviceIntent);
        } else {
            Intent serviceIntent = new Intent(this, MediaService.class);
            startService(serviceIntent);
        }
    }

    public void open_preview(View view) {

    }

    /***
     *推流
     */
    public void add_push_proxy(final View view) {
        Intent intent = new Intent();
        intent.setClass(context, PushActivity.class);
        startActivity(intent);
    }

    /***
     *拉流
     */
    public void add_pull_proxy(final View view) {
        Intent intent = new Intent();
        intent.setClass(context, PullActivity.class);
        startActivity(intent);
    }


    //开始录制
    public void start_record(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // 1.创建Client
                OkHttpClient client = new OkHttpClient.Builder().build();
                // 2.创建Request
                String url = "http://127.0.0.1:8080/index/api/startRecord";
                // 3.构建表单参数
                String stringBuilder = url + "?secret=" + Constant.secret + "&type=1" +
                        "&vhost=" + Constant.vhost +
                        "&app=live" +
                        "&stream=" + Constant.stream_id;
                Request request = new Request.Builder().get().url(stringBuilder).build();
                Log.d(TAG, "请求参数" + stringBuilder);

                // 3.call execute同步
                try {
                    Response response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // 异步，回调在子线程中执行
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.d(TAG, "失败返回值：" + call.toString());
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        Log.d(TAG, "startSendRtp返回值：" + response.body().string());
                    }
                });
            }
        }).start();

    }


}
