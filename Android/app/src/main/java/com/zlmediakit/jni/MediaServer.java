//package com.zlmediakit.jni;
//
//import android.app.Activity;
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.os.Environment;
//import android.os.IBinder;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.widget.Toast;
//
///**
// * @AUTHOR fengyook
// * @DATE 2021/10/28 17:50
// * @DES
// */
//
//public class MediaServer extends Service {
//    private static String[] PERMISSIONS_STORAGE = {
//            "android.permission.READ_EXTERNAL_STORAGE",
//            "android.permission.WRITE_EXTERNAL_STORAGE",
//            "android.permission.INTERNET"};
//
//    Context context;
//    private ZLMediaKit.MediaPlayer _player;
//    private ZLMediaKit.MediaPlayerCallBack mediaPlayerCallBack;
//    private ZLMediaKit.MediaFrame _mediaFrame;
//
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        context = this.getApplicationContext();
//        initZLMediaKit();
//    }
//
//
//    public void initZLMediaKit() {
//        boolean permissionSuccess = true;
//        for (String str : PERMISSIONS_STORAGE) {
//            int permission = ActivityCompat.checkSelfPermission(this, str);
//            if (permission != PackageManager.PERMISSION_GRANTED) {
//                // 没有写的权限，去申请写的权限，会弹出对话框
//                ActivityCompat.requestPermissions((Activity) this.getApplicationContext(), PERMISSIONS_STORAGE, 1);
//                permissionSuccess = false;
//                break;
//            }
//        }
//
//        String sdDir = Environment.getExternalStoragePublicDirectory("").toString();
//        if (permissionSuccess) {
////            Toast.makeText(this, "你可以修改配置文件再启动：" + sd_dir + "/zlmediakit.ini", Toast.LENGTH_LONG).show();
////            Toast.makeText(this, "SSL证书请放置在：" + sd_dir + "/zlmediakit.pem", Toast.LENGTH_LONG).show();
////            start_play();
//        } else {
//            Toast.makeText(this, "请给予我权限，否则无法启动测试！", Toast.LENGTH_LONG).show();
//        }
//        ZLMediaKit.startDemo(sdDir);
//    }
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//
//
//}
